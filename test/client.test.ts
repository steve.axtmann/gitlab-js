import got from 'got';
import faker from 'faker';
import { Client } from '../src';

jest.mock('got');
const mockedGot = got as jest.Mocked<typeof got>;

describe('init', () => {
  beforeEach(async () => {
    delete process.env.GITLAB_TOKEN;
    delete process.env.GITLAB_HOST;
  });

  test('can initialise the library', () => {
    const extend = mockedGot.extend.mockReturnThis();

    const client = new Client();

    expect(client)
      .toBeTruthy();
    expect(extend)
      .toHaveBeenCalledWith({
        prefixUrl: 'https://gitlab.com/api/v4/',
        responseType: 'json',
        headers: {
          'private-token': undefined,
        },
      });
  });

  test('can initialise the library with a token', () => {
    const extend = mockedGot.extend.mockReturnThis();
    const token = faker.git.commitSha();

    const client = new Client(token);

    expect(client)
      .toBeTruthy();
    expect(extend)
      .toHaveBeenCalledWith({
        prefixUrl: 'https://gitlab.com/api/v4/',
        responseType: 'json',
        headers: {
          'private-token': token,
        },
      });
  });

  test('can initialise the library with a config object', () => {
    const extend = mockedGot.extend.mockReturnThis();
    const token = faker.git.commitSha();
    const host = faker.internet.url();

    const client = new Client({
      token,
      host,
    });

    expect(client)
      .toBeTruthy();
    expect(extend)
      .toHaveBeenCalledWith({
        prefixUrl: `${host}/api/v4/`,
        responseType: 'json',
        headers: {
          'private-token': token,
        },
      });
  });

  test('can initialise the library with a host-less config object', () => {
    const extend = mockedGot.extend.mockReturnThis();
    const token = faker.git.commitSha();

    const client = new Client({ token });

    expect(client)
      .toBeTruthy();
    expect(extend)
      .toHaveBeenCalledWith({
        prefixUrl: 'https://gitlab.com/api/v4/',
        responseType: 'json',
        headers: {
          'private-token': token,
        },
      });
  });

  test('handles host being suffixed with /', () => {
    const extend = mockedGot.extend.mockReturnThis();
    const host = `${faker.internet.url()}/`;

    const client = new Client({ host });

    expect(client)
      .toBeTruthy();
    expect(extend)
      .toHaveBeenCalledWith({
        prefixUrl: `${host}api/v4/`,
        responseType: 'json',
        headers: {
          'private-token': undefined,
        },
      });
  });

  test('can initialise the library via the env', () => {
    const extend = mockedGot.extend.mockReturnThis();

    process.env.GITLAB_TOKEN = faker.git.commitSha();
    process.env.GITLAB_HOST = faker.internet.url();

    const client = new Client();

    expect(client)
      .toBeTruthy();
    expect(extend)
      .toHaveBeenCalledWith({
        prefixUrl: `${process.env.GITLAB_HOST}/api/v4/`,
        responseType: 'json',
        headers: {
          'private-token': process.env.GITLAB_TOKEN,
        },
      });
  });
});

describe('get', () => {
  test('can get a resource from the API', async () => {
    const http: any = jest.fn()
      .mockResolvedValue({ body: { a: 'result' } });
    mockedGot.extend.mockReturnValue(http);

    const client = new Client();
    const path = 'some/path';

    const response = await client.get(path);

    expect(http)
      .toHaveBeenCalledWith(path, {
        searchParams: undefined,
      });
    expect(response)
      .toStrictEqual({ a: 'result' });
  });

  test('can get a resource from the API with a query', async () => {
    const http: any = jest.fn()
      .mockResolvedValue({ body: { a: 'result' } });
    mockedGot.extend.mockReturnValue(http);

    const client = new Client();
    const path = 'some/path';
    const query = {
      some: 'query',
    };

    const response = await client.get(path, query);

    expect(http)
      .toHaveBeenCalledWith(path, {
        searchParams: query,
      });
    expect(response)
      .toStrictEqual({ a: 'result' });
  });
});

describe('post', () => {
  test('can post a resource to the API', async () => {
    const http: any = jest.fn()
      .mockResolvedValue({ body: { a: 'result' } });
    mockedGot.extend.mockReturnValue(http);

    const client = new Client();
    const path = 'some/path';
    const query = { some: 'param' };

    const response = await client.post(path, null, query);

    expect(http)
      .toHaveBeenCalledWith(path, {
        method: 'POST',
        body: null,
        searchParams: query,
      });
    expect(response)
      .toStrictEqual({ a: 'result' });
  });

  test('can post a resource to the API with a body', async () => {
    const http: any = jest.fn()
      .mockResolvedValue({ body: { a: 'result' } });
    mockedGot.extend.mockReturnValue(http);

    const client = new Client();
    const path = 'some/path';
    const query = { some: 'param' };

    const response = await client.post(path, query);

    expect(http)
      .toHaveBeenCalledWith(path, {
        method: 'POST',
        body: query,
        searchParams: undefined,
      });
    expect(response)
      .toStrictEqual({ a: 'result' });
  });

  test('can post a simple resource to the API', async () => {
    const http: any = jest.fn()
      .mockResolvedValue({ body: { a: 'result' } });
    mockedGot.extend.mockReturnValue(http);

    const client = new Client();
    const path = 'some/path';

    const response = await client.post(path);

    expect(http)
      .toHaveBeenCalledWith(path, {
        method: 'POST',
        body: undefined,
        searchParams: undefined,
      });
    expect(response)
      .toStrictEqual({ a: 'result' });
  });
});

describe('put', () => {
  test('can put a resource to the API', async () => {
    const http: any = jest.fn()
      .mockResolvedValue({ body: { a: 'result' } });
    mockedGot.extend.mockReturnValue(http);

    const client = new Client();
    const path = 'some/path';
    const query = { some: 'param' };

    const response = await client.put(path, null, query);

    expect(http)
      .toHaveBeenCalledWith(path, {
        method: 'PUT',
        body: null,
        searchParams: query,
      });
    expect(response)
      .toStrictEqual({ a: 'result' });
  });

  test('can put a resource to the API with a body', async () => {
    const http: any = jest.fn()
      .mockResolvedValue({ body: { a: 'result' } });
    mockedGot.extend.mockReturnValue(http);

    const client = new Client();
    const path = 'some/path';
    const query = { some: 'param' };

    const response = await client.put(path, query);

    expect(http)
      .toHaveBeenCalledWith(path, {
        method: 'PUT',
        body: query,
        searchParams: undefined,
      });
    expect(response)
      .toStrictEqual({ a: 'result' });
  });

  test('can put a simple resource to the API', async () => {
    const http: any = jest.fn()
      .mockResolvedValue({ body: { a: 'result' } });
    mockedGot.extend.mockReturnValue(http);

    const client = new Client();
    const path = 'some/path';

    const response = await client.put(path);

    expect(http)
      .toHaveBeenCalledWith(path, {
        method: 'PUT',
        body: undefined,
        searchParams: undefined,
      });
    expect(response)
      .toStrictEqual({ a: 'result' });
  });
});
