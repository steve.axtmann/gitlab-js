# [1.0.0-alpha.11](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.10...v1.0.0-alpha.11) (2021-06-09)


### Bug Fixes

* **merge requests:** toggle draft now uses /draft instead of /wip ([567756a](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/567756a66ef7924ee55a3f2a41f7d6b8be3235dc))

# [1.0.0-alpha.10](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.9...v1.0.0-alpha.10) (2021-04-17)


### Features

* **closes issues:** add api to load the issues that are closed by an MR ([daa21d1](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/daa21d1fd65dc568b51845015e2a05e1c95dbf78))

# [1.0.0-alpha.9](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.8...v1.0.0-alpha.9) (2021-03-23)


### Features

* **labels:** add apis to add/remove labels ([753ffb3](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/753ffb37f46b359ed3977603a6fea37a91b34b44))

# [1.0.0-alpha.8](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.7...v1.0.0-alpha.8) (2021-02-26)


### Features

* **iterations:** added api for iterations ([914593f](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/914593f0e0fa915f929386ee45373d4a7ab84c63))

# [1.0.0-alpha.7](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.6...v1.0.0-alpha.7) (2021-02-18)


### Features

* **collection:** optionally allow overrides to the http options when using save on a collection ([4a75ebb](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/4a75ebb0a94fbed0c266136332967af2ff76219f))
* **discussion:** add notes child to discussions ([48dc412](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/48dc412ca1a24eb1c6ec6d56fbe318e5fa4fea70))

# [1.0.0-alpha.6](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.5...v1.0.0-alpha.6) (2021-02-12)


### Bug Fixes

* **trace:** fix trace including too many parents in the request url & not requesting responseType text ([78e1ccb](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/78e1ccb7df77b1b963b02a70fb66cb7bbfa20100))

# [1.0.0-alpha.5](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.4...v1.0.0-alpha.5) (2021-02-11)


### Bug Fixes

* **apis:** fix export of environment api ([6d19cd5](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/6d19cd5cb76b5a363a7d8732a0ac3c50945cc917))

# [1.0.0-alpha.4](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.3...v1.0.0-alpha.4) (2021-02-11)


### Features

* **apis:** added basic project environments api ([20ea8c9](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/20ea8c99eb915b6af57b153136963b8fe57e62d3))

# [1.0.0-alpha.3](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.2...v1.0.0-alpha.3) (2021-01-21)


### Bug Fixes

* **deploy:** fix new releases not being tagged with latest ([2f32c05](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/2f32c053ef6f2b9457a046685eb0ff79c9799fdd))

# [1.0.0-alpha.2](https://gitlab.com/nerd-vision/opensource/gitlab-js/compare/v1.0.0-alpha.1...v1.0.0-alpha.2) (2021-01-20)


### Bug Fixes

* **client:** fix default host overriding GITLAB_HOST env ([ef33f28](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/ef33f2853d7fb5616700721088c9389dd8b4c5cd))

# 1.0.0-alpha.1 (2021-01-20)


### Features

* initial release ([f5929a9](https://gitlab.com/nerd-vision/opensource/gitlab-js/commit/f5929a973b513fa0c16103d15956b4735f6045e5))
