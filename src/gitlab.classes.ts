// eslint-disable-next-line max-classes-per-file
import got, {
  Got,
  Options,
  ResponseType,
} from 'got';

/**
 * Typing for building a generic item e.g. a Group or Issue
 */
type GenericItemConstructor<T> = (new (
  client: Client,
  data?: any,
) => T) & { namespace: string };

/**
 * Typing for building a singular item e.g. a Trace
 */
type SingularItemConstructor<T> = (new (
  parent: GenericItem,
  data?: any,
) => T) & { namespace: string; onlyRootPath?: boolean; responseType: ResponseType; };

// eslint-disable-next-line
const Labelable = (superClass: typeof GenericItem) => class extends superClass {
  /**
   * Add labels to this merge request
   *
   * @param labels the labels to add
   */
  async addLabel(...labels: string[]) : Promise<void> {
    await this.client.put(this.path, undefined, {
      add_labels: labels.join(','),
    });
  }

  /**
   * Remove labels from this merge request
   *
   * @param labels the labels to remove
   */
  async removeLabel(...labels: string[]) : Promise<void> {
    await this.client.put(this.path, undefined, {
      remove_labels: labels.join(','),
    });
  }
};

export class GenericItem {
  static namespace: string = '';

  parent?: GenericItem;

  rootParent: boolean = false;

  protected itemId: number | null = null;

  constructor(
    protected client: Client,
    public data?: any,
  ) {
    if (data.iid) {
      this.itemId = data.iid;
    } else if (data.id) {
      this.itemId = data.id;
    }
  }

  /**
   * Get the queryable ID for the current item
   *
   * @return The queryable ID
   */
  get id(): number | null {
    return this.itemId;
  }

  /**
   * Build the path for the current item
   *
   * @return The built path
   */
  get namespacedName(): string {
    const encoded = typeof this.id === 'string' ? encodeURIComponent(this.id) : this.id;

    // @ts-ignore
    return `${this.constructor.namespace}/${encoded}`;
  }

  /**
   * Get the path for sending requests
   *
   * @return The path to use
   */
  get path(): string {
    let url = this.namespacedName;

    url = this.prependParentPath(url);

    return url;
  }

  /**
   * Get the parent part of the path for sending requests
   *
   * @return The path to use
   */
  get pathOnlyRoot(): string {
    let { parent } = this;

    const url = this.namespacedName;

    if (!parent) {
      return url;
    }

    while (!parent.rootParent) {
      parent = parent.parent;

      if (!parent) {
        return url;
      }
    }

    return `${parent.namespacedName}/${url}`;
  }

  /**
   * Get the item from GitLab
   *
   * @param params - Option query parameters
   */
  async get(params?: any): Promise<any> {
    return this.client.get(this.path, params);
  }

  /**
   * Get a direct child of the parent
   *
   * @param child - The childs' constructor
   * @param params - Optional query parameters
   *
   * @return The child
   */
  async getSingularChild<T extends SingularItem>(
    child: SingularItemConstructor<T>,
    params?: any,
  ): Promise<T> {
    const response = await this.client.get(
      `${child.onlyRootPath ? this.pathOnlyRoot : this.path}/${child.namespace}`,
      params,
      { responseType: child.responseType } as any,
    );

    // eslint-disable-next-line new-cap
    return new child(this, response);
  }

  /**
   * Prepend the path of the parent to the path
   *
   * @param path - The path to prepend to
   * @private
   *
   * @return The prepended path
   */
  private prependParentPath(path: string): string {
    let { parent } = this;

    while (parent) {
      // eslint-disable-next-line no-param-reassign
      path = `${parent.namespacedName}/${path}`;

      parent = parent.rootParent ? undefined : parent.parent;
    }

    return path;
  }
}

export class SingularItem {
  static namespace: string = '';

  static onlyRootPath: boolean = false;

  static responseType: ResponseType = 'json';

  constructor(
    public parent?: GenericItem,
    public data?: any,
  ) {
  }
}

export class Collection<T extends GenericItem> {
  constructor(
    protected construct: GenericItemConstructor<T>,
    protected client: Client,
    protected parent?: GenericItem,
  ) {
  }

  protected get namespace():string {
    return this.construct.namespace;
  }

  /**
   * List all the children in the parent
   *
   * @param options - Optional query to restrict the results
   *
   * @return A list of children that match the input
   */
  // @ts-ignore
  async list(options: any = {}): Promise<T[]> {
    let url = this.namespace;

    url = this.prependParentPath(url);

    const response = await this.client.get<any>(url, options);

    // convert response which is an array of items to the desired type 'T'
    const mapped = response.map(this.constructChild.bind(this));
    // mapped is now Promise<T>[] so we need to await them as we need Promise<T[]>
    // eslint-disable-next-line no-return-await
    return await Promise.all(mapped);
  }

  /**
   * Find an item from its' ID
   *
   * @param id - The ID to find via it's path or its' numerical ID
   * @param options - An optional query to restrict the results
   *
   * @return The found item
   */
  // @ts-ignore
  async find(
    id: string | number,
    options: any = {},
  ): Promise<T> {
    const encoded = typeof id === 'string' ? encodeURIComponent(id) : id;

    let url = `${this.namespace}/${encoded}`;

    url = this.prependParentPath(url);

    const response = await this.client.get<any>(url, options);

    return this.constructChild(response);
  }

  /**
   * Save the given child to the parent collection
   *
   * @param item - The item/data to save
   * @param options - Optional options to pass to the http client
   *
   * @return The saved item
   */
  async save(
    item: T | { [key: string]: any },
    options?: Options,
  ): Promise<T> {
    let url = this.namespace;
    const id = item instanceof GenericItem ? item.id : item.iid || item.id;

    if (id) {
      const encoded = typeof id === 'string' ? encodeURIComponent(id) : id;

      url += `/${encoded}`;
    }

    url = this.prependParentPath(url);

    const response = await this.client[id ? 'put' : 'post'](
      url,
      undefined,
      item.data ?? item,
      options,
    );

    return this.constructChild(response);
  }

  /**
   * Construct a child of the parent
   *
   * @param rawData - The initial data for the child
   * @private
   *
   * @return The instantiated child
   */
  protected async constructChild(rawData: any): Promise<T> {
    // eslint-disable-next-line new-cap
    const item = new this.construct(this.client, rawData);

    item.parent = this.parent;

    return item;
  }

  /**
   * Prepend the path of the parent to the path
   *
   * @param path - The path to prepend to
   * @private
   *
   * @return The prepended path
   */
  private prependParentPath(path: string): string {
    let { parent } = this;

    while (parent) {
      // eslint-disable-next-line no-param-reassign
      path = `${parent.namespacedName}/${path}`;

      parent = parent.rootParent ? undefined : parent.parent;
    }

    return path;
  }
}

/**
 * This type is used when a collection is loaded from on place, but its items have a logical parent somewhere else.
 *
 * e.g. A MergeRequest can have a list of Issues that it will close, the logical parent of the Issues is the project not the MergeRequest
 * This mapping is requied so the commands on the Issues returned will sill function
 */
export class MappedCollection<T extends GenericItem> extends Collection<T> {
  constructor(protected construct: GenericItemConstructor<T>,
    protected client: Client,
    protected parent?: GenericItem,
    private itemNameSpace?: string) {
    super(construct, client, parent);
  }

  /**
   * Map the item namespace to the name space passed in the constructor
   * @protected
   */
  protected get namespace(): string {
    return this.itemNameSpace ?? this.construct.namespace;
  }

  /**
   * Overrid the construc child call to change the parent to the project
   * @param rawData
   * @protected
   */
  protected async constructChild(rawData: any): Promise<T> {
    const child = await super.constructChild(rawData);

    if (child.data.project_id) {
        child.parent = await this.client.projects.find(child.data.project_id);
    }

    return child;
  }
}

export class Note extends GenericItem {
  static namespace = 'notes';

  parent?: Issue | MergeRequest | Epic | Discussion;
}

export class Discussion extends GenericItem {
  static namespace = 'discussions';

  parent?: Issue | MergeRequest | Epic;

  notes: Collection<Note>;

  constructor(
    client: Client,
    data: any,
  ) {
    super(client, data);

    this.notes = new Collection<Note>(Note, client, this);
  }
}

export class Iteration extends GenericItem {
  static namespace = 'iterations';

  parent?: Group | Project;
}

export class Milestone extends GenericItem {
  static namespace = 'milestones';

  parent?: Group;
}

export class Approvals extends SingularItem {
  static namespace = 'approvals';

  parent?: MergeRequest;
}

export class Issue extends Labelable(GenericItem) {
  static namespace = 'issues';

  parent?: Project;

  discussions: Collection<Discussion>;

  notes: Collection<Note>;

  constructor(
    client: Client,
    data: any,
  ) {
    super(client, data);

    this.discussions = new Collection<Discussion>(Discussion, client, this);
    this.notes = new Collection<Note>(Note, client, this);
  }

  /**
   * Weigh the issue with the given weight
   *
   * @param weight - The weight to give the issue
   */
  async weigh(weight: number): Promise<void> {
    await this.client.put(this.path, undefined, { weight: Math.round(weight) });
  }
}

export class MergeRequest extends Labelable(GenericItem) {
  static namespace = 'merge_requests';

  parent?: Project;

  discussions: Collection<Discussion>;

  notes: Collection<Note>;

  constructor(
    client: Client,
    data: any,
  ) {
    super(client, data);

    this.discussions = new Collection<Discussion>(Discussion, client, this);
    this.notes = new Collection<Note>(Note, client, this);
  }

  /**
   * Toggle the draft status of the MR
   */
  async toggleDraft(): Promise<void> {
    await this.client.post(`${this.path}/notes`, undefined, { body: '/draft' });
  }

  /**
   * Get the approvals for the MR
   *
   * @return The approvals
   */
  async approvals(): Promise<Approvals> {
    return this.getSingularChild(Approvals);
  }

  /**
   * Approve the MR
   *
   * @param params - Parameters to send along with the request
   */
  async approve(params?: {
    sha?: string;
    approval_password?: string;
  }): Promise<void> {
    await this.client.post(`${this.path}/approve`, undefined, params);
  }

  /**
   * Unapprove the MR
   */
  async unapprove(): Promise<void> {
    await this.client.post(`${this.path}/unapprove`);
  }

  /**
   * The list of issues that this MergeRequest will close
   *
   * The Issues returned will have the parents as the project they belong too
   *
   * @return Collection<Issue>
   */
  closesIssues(): Collection<Issue> {
    return new MappedCollection<Issue>(Issue, this.client, this, 'closes_issues');
  }
}

export class Epic extends Labelable(GenericItem) {
  static namespace = 'epics';

  issues: Collection<Issue>;

  discussions: Collection<Discussion>;

  notes: Collection<Note>;

  constructor(
    client: Client,
    data: any,
  ) {
    super(client, data);

    this.issues = new Collection<Issue>(Issue, client, this);
    this.discussions = new Collection<Discussion>(Discussion, client, this);
    this.notes = new Collection<Note>(Note, client, this);
  }
}

export class Branch extends GenericItem {
  static namespace = 'repository/branches';

  parent?: Project;
}

export class Tag extends GenericItem {
  static namespace = 'repository/tags';

  parent?: Project;
}

export class Commit extends GenericItem {
  static namespace = 'repository/commits';

  parent?: Project;
}

export class Trace extends SingularItem {
  static namespace = 'trace';

  static onlyRootPath = true;

  static responseType: ResponseType = 'text';

  parent?: Job;
}

export class Job extends GenericItem {
  static namespace = 'jobs';

  parent?: Project;

  pipeline?: Pipeline;

  /**
   * Get the trace output from the job
   *
   * @return The trace for the job
   */
  async trace(): Promise<Trace> {
    return this.getSingularChild(Trace);
  }
}

export class Pipeline extends GenericItem {
  static namespace = 'pipelines';

  parent?: Project;

  jobs: Collection<Job>;

  constructor(
    client: Client,
    data: any,
  ) {
    super(client, data);

    this.jobs = new Collection<Job>(Job, client, this);
  }
}

export class Release extends GenericItem {
  static namespace = 'releases';

  parent?: Project;
}

export class Board extends GenericItem {
  static namespace = 'boards';

  parent?: Project | Group;
}

export class Environment extends GenericItem {
  static namespace = 'environments';

  parent?: Project;
}

export class Project extends GenericItem {
  static namespace = 'projects';

  rootParent = true;

  parent?: Group;

  issues: Collection<Issue>;

  mergeRequests: Collection<MergeRequest>;

  branches: Collection<Branch>;

  tags: Collection<Tag>;

  pipelines: Collection<Pipeline>;

  jobs: Collection<Job>;

  releases: Collection<Release>;

  commits: Collection<Commit>;

  boards: Collection<Board>;

  environments: Collection<Environment>;

  iterations: Collection<Iteration>;

  constructor(
    client: Client,
    data: any,
  ) {
    super(client, data);

    this.issues = new Collection<Issue>(Issue, client, this);
    this.mergeRequests = new Collection<MergeRequest>(MergeRequest, client, this);
    this.branches = new Collection<Branch>(Branch, client, this);
    this.tags = new Collection<Tag>(Tag, client, this);
    this.pipelines = new Collection<Pipeline>(Pipeline, client, this);
    this.jobs = new Collection<Job>(Job, client, this);
    this.releases = new Collection<Release>(Release, client, this);
    this.commits = new Collection<Commit>(Commit, client, this);
    this.boards = new Collection<Board>(Board, client, this);
    this.environments = new Collection<Environment>(Environment, client, this);
    this.iterations = new Collection<Iteration>(Iteration, client, this);
  }
}

export class Group extends GenericItem {
  static namespace = 'groups';

  parent?: Group;

  projects: Collection<Project>;

  issues: Collection<Issue>;

  mergeRequests: Collection<MergeRequest>;

  epics: Collection<Epic>;

  boards: Collection<Board>;

  milestones: Collection<Milestone>;

  iterations: Collection<Iteration>;

  constructor(
    client: Client,
    data: any,
  ) {
    super(client, data);

    this.projects = new Collection<Project>(Project, client, this);
    this.issues = new Collection<Issue>(Issue, client, this);
    this.mergeRequests = new Collection<MergeRequest>(MergeRequest, client, this);
    this.epics = new Collection<Epic>(Epic, client, this);
    this.boards = new Collection<Board>(Board, client, this);
    this.milestones = new Collection<Milestone>(Milestone, client, this);
    this.iterations = new Collection<Iteration>(Iteration, client, this);
  }
}

export class User extends GenericItem {
  static namespace = 'users';

  parent: undefined;
}

export class Client {
  projects: Collection<Project>;

  mergeRequests: Collection<MergeRequest>;

  groups: Collection<Group>;

  users: Collection<User>;

  issues: Collection<Issue>;

  epics: Collection<Epic>;

  protected http: Got;

  constructor(config?: string | { token?: string; host?: string }) {
    this.projects = new Collection<Project>(Project, this);
    this.mergeRequests = new Collection<MergeRequest>(MergeRequest, this);
    this.groups = new Collection<Group>(Group, this);
    this.users = new Collection<User>(User, this);
    this.issues = new Collection<Issue>(Issue, this);
    this.epics = new Collection<Epic>(Epic, this);

    let token;
    let hostUrl = 'https://gitlab.com';

    if (config) {
      if (typeof config === 'string') {
        token = config;
      } else {
        token = config.token;
        hostUrl = config.host || hostUrl;
      }
    }

    if (!token && process.env.GITLAB_TOKEN) {
      token = process.env.GITLAB_TOKEN;
    }

    if (hostUrl === 'https://gitlab.com' && process.env.GITLAB_HOST) {
      hostUrl = process.env.GITLAB_HOST;
    }

    if (!hostUrl.endsWith('/')) {
      hostUrl += '/';
    }

    hostUrl += 'api/v4/';

    this.http = got.extend({
      prefixUrl: hostUrl,
      responseType: 'json',
      headers: {
        'private-token': token,
      },
    });
  }

  /**
   * Send a GET request to the GitLab instance with the given parameters
   *
   * @param path - The API path to GET from GitLab
   * @param query - An optional object which will be converted to a query string
   * @param options - An optional object of options to pass to got
   *
   * @return The response from GitLab
   */
  async get<T>(
    path: string,
    query?: { [key: string]: any },
    options: Options = {},
  ): Promise<T> {
    const response = await this.http<T>(path, {
      ...options as any,
      searchParams: query,
    });

    return response.body;
  }

  /**
   * Send a POST request to the GitLab instance with the given parameters
   *
   * @param path - The API path to POST to GitLab
   * @param body - The body of the request
   * @param query - An optional object which will be converted to a query string
   * @param options - An optional object of options to pass to got
   *
   * @return The response from GitLab
   */
  async post<T>(
    path: string,
    body?: any,
    query?: { [key: string]: any },
    options: Options = {},
  ): Promise<T> {
    const response = await this.http<T>(path, {
      ...options as any,
      method: 'POST',
      body,
      searchParams: query,
    });

    return response.body;
  }

  /**
   * Send a PUT request to the GitLab instance with the given parameters
   *
   * @param path - The API path to PUT to GitLab
   * @param body - The body of the request
   * @param query - An optional object which will be converted to a query string
   * @param options - An optional object of options to pass to got
   *
   * @return The response from GitLab
   */
  async put<T>(
    path: string,
    body?: any,
    query?: { [key: string]: any },
    options: Options = {},
  ): Promise<T> {
    const response = await this.http<T>(path, {
      ...options as any,
      method: 'PUT',
      body,
      searchParams: query,
    });

    return response.body;
  }
}
